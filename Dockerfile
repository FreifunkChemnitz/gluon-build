FROM gluonmesh/build
ENV BROKEN=1
ENV FORCE_UNSAFE_CONFIGURE=1

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    build-essential \
    ca-certificates \
    curl \
    gawk \
    file \
    git \
    libncurses-dev \
    lua-check \
    python2 \
    shellcheck \
    time \
    unzip \
    wget \
    && rm -rf /var/lib/apt/lists/* && \
    git clone --branch v2020.2.2 https://github.com/freifunk-gluon/gluon.git
WORKDIR /gluon
RUN ln -s /gluon/docs/site-example /gluon/site && \
    make update && \
    make download
